package ph.edu.hau.carcooling;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class APIClient{
    private Context context;
    private RequestQueue queue;
    public APIClient(Context context){
        this.context = context;
        this.queue = Volley.newRequestQueue(context);
    }
}
