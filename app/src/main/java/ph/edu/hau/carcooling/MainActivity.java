package ph.edu.hau.carcooling;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    private final String BASE_URL = "https://cooling-api.herokuapp.com/";
    private static final String TAG = "MainActivity";
    private RequestQueue queue;
    private Gson gson;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this.getApplicationContext();
        this.queue = Volley.newRequestQueue(context);
        gson = new Gson();
        setContentView(R.layout.activity_main);

        startMonitorThread();

        Button toggle = (Button) findViewById(R.id.toggle);
        toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleFan();
            }
        });
        final TextView targetText = (TextView) findViewById(R.id.txtTarget);

        Button target = (Button) findViewById(R.id.btnTarget);
        target.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    setTarget(Float.valueOf(targetText.getText().toString()));
                } catch (Exception ex) {
                    Toast.makeText(MainActivity.this, "Please enter a valid temperature", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void startMonitorThread() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    while (true) {
                        sleep(1000);
                        getStatus();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();
    }

    private void getStatus() {
        request(Request.Method.GET, "/status", null);
    }

    private void toggleFan() {
        request(Request.Method.POST, "/fan/toggle", null);
    }

    private void setTarget(Float target) {
        request(Request.Method.POST, "/temperature/target?targetTemp=" + target,
                "Target temperature set to " + target);
    }

    private void loadFromStatus(Status status) {
        TextView fanStatus = (TextView) findViewById(R.id.tvStatus);
        TextView temp = (TextView) findViewById(R.id.tvTemp);
        EditText targetTemp = (EditText) findViewById(R.id.txtTarget);

        String fanStatusText = "Off";
        if(status.isFanOn()){
            fanStatusText = "Manual On";
        }
        else{
            try {
                float _temp = status.getCarTemperature();
                float _target = status.getTargetTemp();
                if (_temp > _target) {
                    fanStatusText = "On";
                }
            }
            catch (Exception e){
                //do nothing
            }
        }
        fanStatus.setText(fanStatusText);
        temp.setText(String.format(Locale.getDefault(), "%.2f", status.getCarTemperature()));

        if (!targetTemp.isFocused()) {
            targetTemp.setText(String.format(Locale.getDefault(), "%.2f", status.getTargetTemp()));
        }
    }

    void request(int requestMethod, String relativeUrl, final CharSequence message) {
        StringRequest stringRequest = new StringRequest(requestMethod, BASE_URL + relativeUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Status status = gson.fromJson(response, Status.class);
                        loadFromStatus(status);
                        if (message != null)
                            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.wtf(TAG, "error", error);
                    }
                }
        );
        queue.add(stringRequest);
    }

}
