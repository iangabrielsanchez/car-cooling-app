package ph.edu.hau.carcooling;

public class Status{
    private boolean isFanOn;
    private float carTemperature;
    private float targetTemp;

    public boolean isFanOn() {
        return isFanOn;
    }

    public void setFanOn(boolean fanOn) {
        isFanOn = fanOn;
    }

    public float getCarTemperature() {
        return carTemperature;
    }

    public void setCarTemperature(float carTemperature) {
        this.carTemperature = carTemperature;
    }

    public float getTargetTemp() {
        return targetTemp;
    }

    public void setTargetTemp(float targetTemp) {
        this.targetTemp = targetTemp;
    }
}
